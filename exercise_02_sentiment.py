"""Build a sentiment analysis / polarity model

Sentiment analysis can be casted as a binary text classification problem,
that is fitting a linear classifier on features extracted from the text
of the user messages so as to guess wether the opinion of the author is
positive or negative.

In this examples we will use a movie review dataset.

"""
# Author: Olivier Grisel <olivier.grisel@ensta.org>
# License: Simplified BSD


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.datasets import load_files
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.naive_bayes import MultinomialNB


if __name__ == "__main__":
    # NOTE: we put the following in a 'if __name__ == "__main__"' protected
    # block to be able to use a multi-core grid search that also works under
    # Windows, see: http://docs.python.org/library/multiprocessing.html#windows
    # The multiprocessing module is used as the backend of joblib.Parallel
    # that is used when n_jobs != 1 in GridSearchCV

    # the training data folder must be passed as first argument
    movie_reviews_data_folder = r"./data"
    dataset = load_files(movie_reviews_data_folder, shuffle=False)
    print("n_samples: %d" % len(dataset.data))

    # split the dataset in training and test set:
    docs_train, docs_test, y_train, y_test = train_test_split(
        dataset.data, dataset.target, test_size=0.25, random_state=None)

    # TASK: Build a vectorizer / classifier pipeline that filters out tokens
    # that are too rare or too frequent
    text_clf_NB = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', MultinomialNB()),
    ])
    LinearSVC()
    text_clf_NB.fit(docs_train, y_train)  
    predictedNB = text_clf_NB.predict(docs_test)

    print(metrics.classification_report(y_test, predictedNB,
                                        target_names=dataset.target_names))
    
    metrics.confusion_matrix(y_test, predictedNB)

    # TASK: Build a grid search to find out whether unigrams or bigrams are
    # more useful.
    # Fit the pipeline on the training set using grid search for the parameters 
    parameters = {  'vect__ngram_range': [(1, 1), (1, 2), (1, 3), (2, 4)],
                    'tfidf__use_idf': (True, False),
                    'clf__alpha': (1e-2, 1e-3, 1e-4),
                 }

    gs_clf = GridSearchCV(text_clf_NB, parameters, n_jobs=-1)
    gs_clf = gs_clf.fit(docs_train, y_train)


    # TASK: print the cross-validated scores for the each parameters set
    # explored by the grid search
    for param_name in sorted(parameters.keys()):
        print("%s: %r" % (param_name, gs_clf.best_params_[param_name]))


    # TASK: Predict the outcome on the testing set and store it in a variable
    # named y_predicted
    y_predicted = gs_clf.predict(docs_test)

    # Print the classification report
    print(metrics.classification_report(y_test, y_predicted,
                                        target_names=dataset.target_names))

    # Print and plot the confusion matrix
    cm = metrics.confusion_matrix(y_test, y_predicted)
    print(cm)


    text_clf_SVC = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', LinearSVC()),
    ])

    text_clf_SVC.fit(docs_train, y_train)
    
    parameters_SVC = {'penalty': [('l1','l2')]}
    
    gs_clf_SVC = GridSearchCV(text_clf_SVC, parameters_SVC, n_jobs=-1, cv = 5)
gs_clf_SVC = gs_clf_SVC.fit(docs_train, y_train)
    